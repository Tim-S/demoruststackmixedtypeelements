///
/// Run with
/// ```sh
/// cargo test --nocapture
/// ```
/// to see the output
#[cfg(test)]
pub mod tests {
    use crate::{Push, Pop, Visit, Text};

    #[test]
    pub fn push_works() {
        let items =
            ()
                .push(5)
                .push(true)
                .push(2)
                .push(3)
                .push(false)
            ;
        println!("{:#?}", items);
    }

    #[test]
    pub fn pop_works(){
        let items = ()
            .push(5)
            .push(true)
            .push(2)
            .push("Hello,");

        let (value, items) = items.pop(); println!("{:?}", value);
        let (value, items) = items.pop(); println!("{:?}", value);
        let (value, items) = items.pop(); println!("{:?}", value);
        println!("{:?}", items);
    }

    #[test]
    pub fn visit_works(){
        let items = ()
            .push(5)
            .push(true)
            .push(2)
            .push(
                ().push("My").push(17))
            .push("Hello,")
            .push("stop");
        println!("{:#?}", items.visit(Text(String::new())));

        let mut items = ().push("Hello,").push(1);
        println!("{:#?}", items.visit(Text(String::new())));
    }
}

trait Push<X> {
    type Next;
    fn push(self, other: X) -> Self::Next;
}

impl<X> Push<X> for () {
    type Next = (X,);

    fn push(self, other: X) -> Self::Next {
        (other,)
    }
}
impl<T1, X> Push<X> for (T1,) {
    type Next = (T1, X);

    fn push(self, other: X) -> Self::Next {
        (self.0, other)
    }
}

impl<T1, T2, X> Push<X> for (T1, T2) {
    type Next = ((T1, T2), X);

    fn push(self, other: X) -> Self::Next {
        (
            self,
            other,
        )
    }
}

trait Pop<X> {
    type Next;
    fn pop(self) -> (X, Self::Next);
}

impl<T1, X> Pop<X> for (T1, X) {
    type Next = T1;

    fn pop(self) -> (X, Self::Next) {
        (self.1, self.0,)
    }
}

impl<X> Pop<X> for (X,) {
    type Next = ();

    fn pop(self) -> (X, Self::Next) {
        (self.0, ())
    }
}

trait Visit<A> {
    fn visit(self, acc: A) -> A;
}

impl<A, T1> Visit<A> for (T1,)
    where T1: Visit<A>
{
    fn visit(self, acc: A) -> A {
        self.0.visit(acc)
    }
}

impl<A, T1, T2> Visit<A> for (T1, T2)
    where
        T1: Visit<A>,
        T2: Visit<A>
{
    fn visit(self, acc: A) -> A {
        let acc = self.0.visit(acc);
        self.1.visit(acc)
    }
}

#[derive(Debug)]
struct Text(String);

impl Visit<Text> for bool {
    fn visit(self, Text (mut s): Text) -> Text {
        match self {
            true => s.push_str("(true)"),
            false => s.push_str("(false)"),
        }
        Text(s)
    }
}

impl Visit<Text> for i32 {
    fn visit(self, Text (mut s): Text) -> Text {
        s.push_str(&format!("(i32 value {})", self));
        Text(s)
    }
}

impl<'a> Visit<Text> for &'a str {
    fn visit(self, Text (mut s): Text) -> Text {
        s.push_str(&format!("({})", self));
        Text(s)
    }
}
